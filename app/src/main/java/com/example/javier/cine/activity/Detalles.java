package com.example.javier.cine.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.javier.cine.R;
import com.example.javier.cine.modelos.Pelicula;
import com.squareup.picasso.Picasso;

public class Detalles extends ActionBarActivity {
    public ImageView imgpelicula;
    public TextView titlepelicula;
    public TextView estreno;
    public TextView descripcion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        titlepelicula=(TextView)findViewById(R.id.titlePelicula);
        estreno=(TextView)findViewById(R.id.estreno);
        descripcion=(TextView)findViewById(R.id.descripcion);
        imgpelicula=(ImageView)findViewById(R.id.imgPelicula);

        titlepelicula.setText(Pelicula.getInstancia().getTitulo()+"");
        estreno.setText("Fecha de estreno: "+Pelicula.getInstancia().getFechaEstreno()+"");
        descripcion.setText(Pelicula.getInstancia().getSinopsis() + "");
        Picasso.with(this.getApplication()).load(Pelicula.getInstancia().getImage()+"").resize(100, 100).into(imgpelicula);
        Log.e("Javier", Pelicula.getInstancia().getImage());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detalles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
