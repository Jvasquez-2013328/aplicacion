package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class TipoSala {

    public String id;
    public String nombre;
    public String descripcion;
    public static TipoSala instancia;

    public static TipoSala getInstancia(){
        if(instancia==null){
            instancia= new TipoSala();
        }
        return  instancia;
    }

    public TipoSala() {
    }

    public TipoSala(String id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static void setInstancia(TipoSala instancia) {
        TipoSala.instancia = instancia;
    }
}
