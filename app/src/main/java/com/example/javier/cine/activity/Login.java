package com.example.javier.cine.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.javier.cine.R;
import com.parse.LogInCallback;
import com.parse.ParseUser;
import com.parse.ParseException;

public class Login extends ActionBarActivity {
    Button btnLogin,btnRegistrar;
    EditText txtUsuarios,txtPassw;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        txtUsuarios = (EditText)findViewById(R.id.txtUsuario);
        txtPassw = (EditText)findViewById(R.id.txtPass);
        btnRegistrar.setOnClickListener(new View.OnClickListener(){
            public void onClick (View view){
                try {
                    Intent myIntent = new Intent(view.getContext(), Registrar.class);

                    startActivityForResult(myIntent, 0);
                }catch (NullPointerException e){

                }

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener(){
            public  void onClick (View view) {
                String usuario = txtUsuarios.getText().toString();
                String pass = txtPassw.getText().toString();
                Log.e("Javier",usuario+" "+pass);
                ParseUser.logInInBackground(usuario, pass,
                        new LogInCallback() {
                            public void done(ParseUser user, ParseException e) {

                                if (user != null) {
                                    // If user exist and authenticated, send user to Welcome.class
                                    Intent intent = new Intent(
                                            Login.this,
                                            MainActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(),
                                            "Successfully Logged in",
                                            Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "No such user exist, please signup",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    protected void onDestroy() {
        super.onDestroy();
    }
}
