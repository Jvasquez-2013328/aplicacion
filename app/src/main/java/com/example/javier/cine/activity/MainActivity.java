package com.example.javier.cine.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javier.cine.R;
import com.example.javier.cine.fragment.CineFragment;
import com.example.javier.cine.fragment.FavoritosFragment;
import com.example.javier.cine.fragment.HorariosFragment;
import com.example.javier.cine.fragment.InicioFragment;
import com.example.javier.cine.fragment.NavigationDrawerFragment;
import com.example.javier.cine.fragment.PeliculaFragment;


import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;

public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener{

    private Toolbar wToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        wToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(wToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment =(NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_fragment);

        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), wToolbar, R.id.navigation_fragment);
        drawerFragment.setwDrawerListener(this);
        ParseUser currentUser = ParseUser.getCurrentUser();


        // Set the currentUser String into TextView
        Toast.makeText(MainActivity.this,"You are logged in as " + ParseUser.getCurrentUser().getUsername(),Toast.LENGTH_SHORT).show();
        // Locate Button in welcome.xml

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.segundoInicio,new InicioFragment());
        fragmentTransaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salir) {
            startActivity(new Intent(MainActivity.this, Login.class));
            ParseUser.logOut();
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position){
            case 1:
                fragment = new InicioFragment();
                title = "Estrenos";
                break;
            case 2:
                fragment = new PeliculaFragment();
                title = "Pelicula";
                break;
            case 3:
                fragment = new FavoritosFragment();
                title = "Favoritos";
                break;
            case 4:
                fragment = new CineFragment();
                title = "Cines";
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.segundoInicio, fragment);
            fragmentTransaction.commit();

            getSupportActionBar().setTitle(title);
        }

    }
}
