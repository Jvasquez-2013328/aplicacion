package com.example.javier.cine.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
/**
 * Created by Javier on 07/06/2015.
 */
public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener{

   private OnItemClickListener wListener;

    GestureDetector wGestureDetector;

    public RecyclerItemClickListener(Context context, OnItemClickListener wListener) {
        this.wListener = wListener;
        wGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){

           @Override
        public boolean onSingleTapUp(MotionEvent e){
                return true;
           }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && wListener != null && wGestureDetector.onTouchEvent(e)){
            wListener.onItemClick(childView, rv.getChildPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    public interface OnItemClickListener{
        public void onItemClick(View view, int position);
    }
}
