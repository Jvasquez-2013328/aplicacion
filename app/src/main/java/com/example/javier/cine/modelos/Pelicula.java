package com.example.javier.cine.modelos;
/**
 * Created by javier on 24/07/15.
 */
public class Pelicula {

    public int id;
    public String titulo;
    public String sinopsis;
    public String trailer_url;
    public String image;
    public String fechaEstreno;
    public String rated;
    public String genero;
    public static Pelicula instancia;

    public static Pelicula getInstancia(){
        if(instancia==null){
            instancia= new Pelicula();
        }
        return  instancia;
    }

    public Pelicula() {
    }

    public Pelicula(int id, String titulo, String sinopsis, String trailer_url, String image, String fechaEstreno, String rated, String genero) {
        this.id = id;
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.trailer_url = trailer_url;
        this.image = image;
        this.fechaEstreno = fechaEstreno;
        this.rated = rated;
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(String fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public static void setInstancia(Pelicula instancia) {
        Pelicula.instancia = instancia;
    }
}