package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class Cine {

    public String id;
    public String nombre;
    public String direccion;
    public String telefono;
    public String latitud;
    public String longitud;
    public String hora_apertura;
    public String hora_cierre;
    public static Cine instancia;

    public static Cine getInstancia(){
        if(instancia==null){
            instancia= new Cine();
        }
        return  instancia;
    }

    public Cine() {
    }

    public Cine(String id, String nombre, String direccion, String telefono, String latitud, String longitud, String hora_apertura, String hora_cierre) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.latitud = latitud;
        this.longitud = longitud;
        this.hora_apertura = hora_apertura;
        this.hora_cierre = hora_cierre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getHora_apertura() {
        return hora_apertura;
    }

    public void setHora_apertura(String hora_apertura) {
        this.hora_apertura = hora_apertura;
    }

    public String getHora_cierre() {
        return hora_cierre;
    }

    public void setHora_cierre(String hora_cierre) {
        this.hora_cierre = hora_cierre;
    }
}
