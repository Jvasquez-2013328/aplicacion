package com.example.javier.cine.modelos;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by javier on 25/07/2015.
 */
public interface PeliculaService {
    @GET("/peliculas")
    void getPelicula(Callback<List<Pelicula>> callback);
}
