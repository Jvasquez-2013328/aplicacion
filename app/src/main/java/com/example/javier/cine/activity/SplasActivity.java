package com.example.javier.cine.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.javier.cine.Application.ParseApplication;
import com.example.javier.cine.R;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;


public class SplasActivity extends ActionBarActivity {

    private static final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splas);

        // Add your initialization code here
        Parse.initialize(this, "ag2SCe70WcxyfzIRC9UHlD1YUFiCOyNMSXdSgyU4", "llxRuAVWN3ggFWoyJKAvNExcUHGsTndLVnBknHXR");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
                    // If user is anonymous, send the user to LoginSignupActivity.class
                    Intent intent = new Intent(SplasActivity.this,
                            Login.class);
                    startActivity(intent);
                    finish();
                } else {
                    ParseUser currentUser = ParseUser.getCurrentUser();
                    if (currentUser != null) {
                        // Send logged in users to Welcome.class
                        Intent intent = new Intent(SplasActivity.this, Login.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Send user to LoginSignupActivity.class
                        Intent intent = new Intent(SplasActivity.this,
                                Login.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
