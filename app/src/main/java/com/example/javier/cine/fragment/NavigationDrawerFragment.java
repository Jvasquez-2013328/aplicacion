package com.example.javier.cine.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.javier.cine.R;
import com.example.javier.cine.adapter.NavigationDrawerAdapter;
import com.example.javier.cine.activity.RecyclerItemClickListener;
import com.parse.ParseUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {


    private ActionBarDrawerToggle wDrawerToggle;
    private DrawerLayout wDrawerLayout;
    private Toolbar wToolbar;
    private RecyclerView wRecycleView;
    private RecyclerView.Adapter wAdapter;
    private RecyclerView.LayoutManager mLayountManager;
    private View containerView;
    private FragmentDrawerListener  wDrawerListener;

    private int ICONS[] = {R.drawable.ic_home, R.drawable.ic_pelicula, R.drawable.ic_favorito, R.drawable.ic_action };
    private  String TITLES[] = {"Inicio", "Peliculas", "Favoritos", "Horarios"};
    private String NAME = "Cargando...";
    private String EMAIL = "Cargando...";
    private int PROFILE = R.mipmap.profile;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        wRecycleView = (RecyclerView) v.findViewById(R.id.RecycleView);
        wRecycleView.setHasFixedSize(true);
        mLayountManager = new LinearLayoutManager(getActivity());
        wRecycleView.setLayoutManager(mLayountManager);
        wAdapter = new NavigationDrawerAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE);
        wRecycleView.setAdapter(wAdapter);

        wRecycleView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                wDrawerListener.onDrawerItemSelected(view, position);
                wDrawerLayout.closeDrawer(containerView);
            }
        }));

        return v;
    }


    public void setUp(DrawerLayout drawerLaout, Toolbar toolbar, int fragmentId) {
        this.wDrawerLayout = drawerLaout;
        this.wToolbar = toolbar;
        this.containerView =  getActivity().findViewById(fragmentId);

        wDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLaout, toolbar, R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                ParseUser currentUser = ParseUser.getCurrentUser();
                TextView usuario = (TextView)drawerView.findViewById(R.id.name);
                TextView correo = (TextView)drawerView.findViewById(R.id.email);
                usuario.setText(currentUser.getUsername()+"");
                correo.setText(currentUser.getEmail()+"");

                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }


        };

        wDrawerLayout.setDrawerListener(wDrawerToggle);
        wDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                wDrawerToggle.syncState();
            }
        });
    }


    public void setwDrawerListener(FragmentDrawerListener drawerListener){
        this.wDrawerListener = drawerListener;
    }



    public interface FragmentDrawerListener{
        public void onDrawerItemSelected(View view, int position);
    }
}
