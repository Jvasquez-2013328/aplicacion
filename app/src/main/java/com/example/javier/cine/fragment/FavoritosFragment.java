package com.example.javier.cine.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javier.cine.R;
import com.example.javier.cine.activity.Detalles;
import com.example.javier.cine.helpers.PeliculasHelper;
import com.example.javier.cine.modelos.Pelicula;
import com.example.javier.cine.modelos.PeliculaService;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {

    public ListView lvPelicula;
    private SQLiteDatabase db;

    public FavoritosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoritos, container, false);
        PeliculasHelper peliculaDB =
                new PeliculasHelper(getActivity(), "Peliculas", null, 1);
        final List<Pelicula> peliculasFavoritos = new ArrayList<Pelicula>();
        lvPelicula = (ListView) view.findViewById(R.id.lvFavoritos);
        db = peliculaDB.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT idPelicula, titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero FROM Pelicula", null);
        if (c.moveToFirst()) {
            do {
                Log.e("Javier","Dato "+ c.getInt(0) +" "+ c.getString(1) +" "+ c.getString(2) +" "+ c.getString(3) +" "+ c.getString(4) +" "+ c.getString(5) +" "+ c.getString(6) +" "+ c.getString(7) +" ");
                peliculasFavoritos.add(new Pelicula(c.getInt(0),c.getString(1)+"",c.getString(2)+"",c.getString(3)+"",c.getString(4)+"",c.getString(5)+"",c.getString(6)+"",c.getString(7)+""));
            } while (c.moveToNext());
        }
        if (c.moveToFirst()) {
            AdaptadorPeliculas adaptadorPeliculas = new AdaptadorPeliculas(getActivity(), peliculasFavoritos);
            lvPelicula.setAdapter(adaptadorPeliculas);
        }else{
            Toast.makeText(getActivity(),"No hay favoritos",Toast.LENGTH_SHORT).show();
        }

        lvPelicula = (ListView) view.findViewById(R.id.lvFavoritos);
        lvPelicula.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pelicula pelicula = (Pelicula) parent.getItemAtPosition(position);
                Pelicula.getInstancia().setId(pelicula.getId());
                Pelicula.getInstancia().setTitulo(pelicula.getTitulo());
                Pelicula.getInstancia().setSinopsis(pelicula.getSinopsis());
                Pelicula.getInstancia().setTrailer_url(pelicula.getTrailer_url());
                Pelicula.getInstancia().setFechaEstreno(pelicula.getFechaEstreno());
                Pelicula.getInstancia().setRated(pelicula.getRated());
                Pelicula.getInstancia().setGenero(pelicula.getGenero());
                Pelicula.getInstancia().setImage(pelicula.getImage());

                startActivity(new Intent(getActivity(), Detalles.class));
            }
        });

        lvPelicula.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Pelicula pelicula = (Pelicula) parent.getItemAtPosition(position);
                    peliculasFavoritos.clear();
                    Integer[] args = new Integer[]{pelicula.id};
                    db.execSQL("DELETE FROM Pelicula WHERE idPelicula = ?", args);
                    Toast.makeText(getActivity(), "Eliminado de favoritos", Toast.LENGTH_SHORT).show();

                    Cursor c = db.rawQuery("SELECT idPelicula, titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero FROM Pelicula", null);
                    if (c.moveToFirst()) {
                        do {
                            Log.e("Javier", "Dato " + c.getInt(0) + " " + c.getString(1) + " " + c.getString(2) + " " + c.getString(3) + " " + c.getString(4) + " " + c.getString(5) + " " + c.getString(6) + " " + c.getString(7) + " ");
                            peliculasFavoritos.add(new Pelicula(c.getInt(0), c.getString(1) + "", c.getString(2) + "", c.getString(3) + "", c.getString(4) + "", c.getString(5) + "", c.getString(6) + "", c.getString(7) + ""));
                        } while (c.moveToNext());
                    }
                    AdaptadorPeliculas adaptadorPeliculas = new AdaptadorPeliculas(getActivity(), peliculasFavoritos);
                    lvPelicula.setAdapter(adaptadorPeliculas);
                }catch (IndexOutOfBoundsException e){
                    Toast.makeText(getActivity(),"No hay favoritos",Toast.LENGTH_SHORT).show();
                }catch (NullPointerException e){
                    Toast.makeText(getActivity(),"No hay favoritos",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        return view;
    }

    public class AdaptadorPeliculas extends ArrayAdapter<Pelicula> {
        private List<Pelicula> listaPeliculas;

        public AdaptadorPeliculas(Context context, List<Pelicula> listaPelicula) {
            super(context, R.layout.peliculas_item, listaPelicula);
            listaPeliculas = listaPelicula;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View item = layoutInflater.inflate(R.layout.peliculas_item, null);

            ImageView imageView = (ImageView) item.findViewById(R.id.imgPelicula);
            TextView titlePelicula = (TextView) item.findViewById(R.id.titlePelicula);
            TextView estreno = (TextView) item.findViewById(R.id.estreno);
            Picasso.with(getContext()).load(listaPeliculas.get(position).getImage()).resize(100, 100).into(imageView);
            titlePelicula.setText(listaPeliculas.get(position).getTitulo());
            estreno.setText(listaPeliculas.get(position).getFechaEstreno());

            return item;
        }
    }
}