package com.example.javier.cine.modelos;

/**
 * Created by javier on 11/06/2015.
 */
public class Usuario {

        private String nombreUsuario;

        private String pass;

        public String getNombreUsuario() {
            return nombreUsuario;
        }

        public void setNombreUsuario(String nombreUsuario) {
            this.nombreUsuario = nombreUsuario;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }


        public Usuario(String nombreUsuario ,String pass){
            this.nombreUsuario = nombreUsuario;

            this.pass = pass;
        }

    }


