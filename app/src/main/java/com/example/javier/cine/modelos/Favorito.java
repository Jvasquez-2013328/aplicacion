package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class Favorito {

    public String idPelicula;
    public String idUsuario;
    public static Favorito instancia;

    public static Favorito getInstancia(){
        if(instancia==null){
            instancia= new Favorito();
        }
        return  instancia;
    }

    public Favorito() {
    }

    public Favorito(String idPelicula, String idUsuario) {
        this.idPelicula = idPelicula;
        this.idUsuario = idUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(String idPelicula) {
        this.idPelicula = idPelicula;
    }
}
