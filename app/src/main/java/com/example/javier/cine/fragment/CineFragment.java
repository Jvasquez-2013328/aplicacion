package com.example.javier.cine.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.javier.cine.R;
import com.example.javier.cine.modelos.Cine;
import com.example.javier.cine.modelos.CineService;
import com.example.javier.cine.modelos.PeliculaService;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CineFragment extends Fragment {
    public ListView lvCine;


    public CineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cine, container, false);
        lvCine = (ListView) view.findViewById(R.id.lvCine);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.1.54/CineK/public").build();
        CineService service = restAdapter.create(CineService.class);
        service.getCine(new Callback<List<Cine>>() {
            @Override
            public void success(List<Cine> cines, Response response) {
                Log.e("Javier", "success");
                AdaptadorCine adaptadorCine = new AdaptadorCine(getActivity(), cines);
                lvCine.setAdapter(adaptadorCine);
            }
            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Javier", String.valueOf(retrofitError));
            }
        });
        return view;
    }

    public class AdaptadorCine extends ArrayAdapter<Cine> {
        private List<Cine> listaCine;

        public AdaptadorCine(Context context, List<Cine> listaCine) {
            super(context, R.layout.cine_item, listaCine);
            this.listaCine = listaCine;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View item = layoutInflater.inflate(R.layout.cine_item, null);

            TextView nombrecine = (TextView) item.findViewById(R.id.nombrecine);
            TextView telefonocine = (TextView) item.findViewById(R.id.telefonocine);
            TextView direccioncine = (TextView) item.findViewById(R.id.direccioncine);
            TextView horariocine = (TextView) item.findViewById(R.id.horariocine);
            nombrecine.setText(""+listaCine.get(position).getNombre());
            telefonocine.setText("Telefono: "+listaCine.get(position).getTelefono());
            direccioncine.setText("Direccion: "+listaCine.get(position).getDireccion());
            horariocine.setText("Horario: "+listaCine.get(position).getHora_apertura()+" - "+listaCine.get(position).getHora_cierre());

            return item;
        }
    }
}