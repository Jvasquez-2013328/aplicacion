package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class FormatoPelicula {

    public String id;
    public String nombre;
    public String descripcion;
    public static FormatoPelicula instancia;

    public static FormatoPelicula getInstancia(){
        if(instancia==null){
            instancia= new FormatoPelicula();
        }
        return  instancia;
    }

    public FormatoPelicula() {
    }

    public FormatoPelicula(String id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
