package com.example.javier.cine.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javier.cine.R;
import com.example.javier.cine.activity.Detalles;
import com.example.javier.cine.helpers.PeliculasHelper;
import com.example.javier.cine.modelos.Pelicula;
import com.example.javier.cine.modelos.PeliculaService;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PeliculaFragment extends Fragment {


    public ListView lvPelicula;
    private SQLiteDatabase db;

    public PeliculaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pelicula, container, false);
        lvPelicula = (ListView) view.findViewById(R.id.lvPelicula);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.1.54/CineK/public").build();
        PeliculaService service = restAdapter.create(PeliculaService.class);
        service.getPelicula(new Callback<List<Pelicula>>() {
            @Override
            public void success(List<Pelicula> peliculas, Response response) {
                Log.e("Javier", "success");
                ArrayList<Pelicula> peliculasCartelera = new ArrayList<Pelicula>();

                Date fechaActual = new Date();
                SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
                String fechaSistema = formateador.format(fechaActual);
                try {
                    fechaActual = formateador.parse(fechaSistema);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                for (int p = 0; p < peliculas.size(); p++) {
                    try {
                        Date fechaPelicula = formateador.parse(peliculas.get(p).getFechaEstreno());
                        Log.e("Javier","Cambio fecha");
                        if (fechaPelicula.before(fechaActual)) {
                            Log.e("Javier","Comparacion ok");
                            peliculasCartelera.add(new Pelicula(peliculas.get(p).getId(),
                                    peliculas.get(p).getTitulo(),
                                    peliculas.get(p).getSinopsis(),
                                    peliculas.get(p).getTrailer_url(),
                                    peliculas.get(p).getImage(),
                                    peliculas.get(p).getFechaEstreno(),
                                    peliculas.get(p).getRated(),
                                    peliculas.get(p).getGenero()));

                        }
                    } catch (ParseException e) {
                        Log.e("Javier Parse", String.valueOf(e) + " " + p);
                    } catch (NullPointerException e) {
                        Log.e("Javier Null", String.valueOf(e) + " " + p);
                    }
                }

                AdaptadorPeliculas adaptadorPeliculas = new AdaptadorPeliculas(getActivity(), peliculasCartelera);
                lvPelicula.setAdapter(adaptadorPeliculas);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Javier", String.valueOf(retrofitError));
            }
        });
        lvPelicula.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Pelicula pelicula = (Pelicula) parent.getItemAtPosition(position);
                 Boolean agregar = true;
                PeliculasHelper peliculaDB =
                        new PeliculasHelper(getActivity(), "Peliculas", null, 1);
                db = peliculaDB.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT idPelicula, titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero FROM Pelicula", null);
                if (c.moveToFirst()) {
                    do {
                        if(c.getInt(0)==pelicula.getId()){
                            agregar=false;
                        }
                    } while (c.moveToNext());
                }
                if(agregar==true){
                    ContentValues nuevoRegistro = new ContentValues();
                    nuevoRegistro.put("idPelicula", pelicula.id+"");
                    nuevoRegistro.put("titulo", pelicula.titulo+"");
                    nuevoRegistro.put("sinopsis", pelicula.sinopsis+"");
                    nuevoRegistro.put("trailer_url",pelicula.trailer_url+"");
                    nuevoRegistro.put("image", pelicula.image+"");
                    nuevoRegistro.put("fechaEstreno", pelicula.fechaEstreno+"");
                    nuevoRegistro.put("rated", pelicula.rated+"");
                    nuevoRegistro.put("genero", pelicula.genero+"");
                    Log.e("Javier", pelicula.id + " "+pelicula.titulo+" "+pelicula.sinopsis+" "+pelicula.trailer_url+" "
                            +pelicula.image+" "+pelicula.fechaEstreno+" "+pelicula.rated+" "+pelicula.genero+" ");
                    db.insert("Pelicula", null, nuevoRegistro);
                    Toast.makeText(getActivity(), "Agregado a favoritos", Toast.LENGTH_SHORT).show();
                }else{
                    Integer[] args = new Integer[]{pelicula.id};
                    db.execSQL("DELETE FROM Pelicula WHERE idPelicula = ?", args);
                    Toast.makeText(getActivity(), "Eliminado de favoritos", Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });
        lvPelicula.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pelicula pelicula = (Pelicula) parent.getItemAtPosition(position);
                Pelicula.getInstancia().setId(pelicula.getId());
                Pelicula.getInstancia().setTitulo(pelicula.getTitulo());
                Pelicula.getInstancia().setSinopsis(pelicula.getSinopsis());
                Pelicula.getInstancia().setTrailer_url(pelicula.getTrailer_url());
                Pelicula.getInstancia().setFechaEstreno(pelicula.getFechaEstreno());
                Pelicula.getInstancia().setRated(pelicula.getRated());
                Pelicula.getInstancia().setGenero(pelicula.getGenero());
                Pelicula.getInstancia().setImage(pelicula.getImage());

                startActivity(new Intent(getActivity(), Detalles.class));
            }
        });
        return view;
    }

    public class AdaptadorPeliculas extends ArrayAdapter<Pelicula> {
        private List<Pelicula> listaPeliculas;

        public AdaptadorPeliculas(Context context, List<Pelicula> listaPelicula) {
            super(context, R.layout.peliculas_item, listaPelicula);
            listaPeliculas = listaPelicula;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View item = layoutInflater.inflate(R.layout.peliculas_item, null);

            ImageView imageView = (ImageView) item.findViewById(R.id.imgPelicula);
            TextView titlePelicula = (TextView) item.findViewById(R.id.titlePelicula);
            TextView estreno = (TextView) item.findViewById(R.id.estreno);
            Picasso.with(getContext()).load(listaPeliculas.get(position).getImage()).resize(100, 100).into(imageView);
            titlePelicula.setText(listaPeliculas.get(position).getTitulo());
            estreno.setText(listaPeliculas.get(position).getFechaEstreno());

            return item;
        }
    }
}