package com.example.javier.cine.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.javier.cine.R;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ParseException;

public class Registrar extends ActionBarActivity {
    EditText txtUsuarioR,txtPassR,txtPassVerificar,txtNombreR,txtCorreoR;
    Button btnRegistrarUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        btnRegistrarUsuario= (Button) findViewById(R.id.btnRegistrarUsuario);
        txtUsuarioR = (EditText) findViewById(R.id.txtUsuarioRegistrar);
        txtPassR = (EditText) findViewById(R.id.txtPassRegistrar);
        txtPassVerificar = (EditText) findViewById(R.id.txtVerificarPass);
        txtCorreoR =(EditText) findViewById(R.id.txtCorreo);
        txtNombreR =(EditText)findViewById(R.id.txtCorreo);

        try{
            btnRegistrarUsuario.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (txtUsuarioR.getText().toString().matches("") || txtPassR.getText().toString().matches("") || txtPassVerificar.getText().toString().matches("")|| txtNombreR.getText().toString().matches("")|| txtCorreoR.getText().toString().matches("")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Ingrese bien los datos", Toast.LENGTH_LONG);
                        toast.show();

                    } else if (txtPassR.getText().toString().matches(txtPassVerificar.getText().toString())) {

                        ParseUser user = new ParseUser();
                        user.setUsername(txtUsuarioR.getText().toString());
                        user.setPassword(txtPassVerificar.getText().toString());
                        user.setEmail(txtCorreoR.getText().toString());
                        user.signUpInBackground(new SignUpCallback() {
                            public void done(ParseException e) {
                                if (e == null) {
                                    // Show a simple Toast message upon successful registration
                                    Toast.makeText(getApplicationContext(),
                                            "Successfully Signed up, please log in.",
                                            Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Sign up Error", Toast.LENGTH_LONG)
                                            .show();
                                }
                            }
                        });

                    } else if (txtPassR.getText().toString() != txtPassVerificar.getText().toString()) {
                        Toast t = Toast.makeText(getApplicationContext(), "Las Contraseñas no son iguales", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            });
        }catch (NullPointerException e){

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registrar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }
}
