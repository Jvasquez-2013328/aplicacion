package com.example.javier.cine.Application;

import android.app.Application;
import com.parse.Parse;
import com.parse.ParseACL;

import com.parse.ParseUser;

public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here
        Parse.initialize(this, "ag2SCe70WcxyfzIRC9UHlD1YUFiCOyNMSXdSgyU4", "llxRuAVWN3ggFWoyJKAvNExcUHGsTndLVnBknHXR");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }

}