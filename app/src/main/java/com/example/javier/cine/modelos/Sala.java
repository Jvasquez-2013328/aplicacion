package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class Sala {

    public String id;
    public String cine_id;
    public String numero;
    public String tiposala_id;
    public static Sala instancia;

    public static Sala getInstancia(){
        if(instancia==null){
            instancia= new Sala();
        }
        return  instancia;
    }

    public Sala() {
    }

    public Sala(String id, String cine_id, String numero, String tiposala_id) {
        this.id = id;
        this.cine_id = cine_id;
        this.numero = numero;
        this.tiposala_id = tiposala_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCine_id() {
        return cine_id;
    }

    public void setCine_id(String cine_id) {
        this.cine_id = cine_id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTiposala_id() {
        return tiposala_id;
    }

    public void setTiposala_id(String tiposala_id) {
        this.tiposala_id = tiposala_id;
    }

}
