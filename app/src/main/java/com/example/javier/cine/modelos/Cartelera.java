package com.example.javier.cine.modelos;

/**
 * Created by javier on 25/07/2015.
 */
public class Cartelera {

    public String id;
    public String sala_id;
    public String pelicula_id;
    public String formatopelicula_id;
    public String formato_lenguaje;
    public String fecha;
    public String hora;

    public static Cartelera instancia;

    public static Cartelera getInstancia(){
        if(instancia==null){
            instancia= new Cartelera();
        }
        return  instancia;
    }

    public Cartelera() {
    }

    public Cartelera(String id, String sala_id, String pelicula_id, String formatopelicula_id, String formato_lenguaje, String fecha, String hora) {
        this.id = id;
        this.sala_id = sala_id;
        this.pelicula_id = pelicula_id;
        this.formatopelicula_id = formatopelicula_id;
        this.formato_lenguaje = formato_lenguaje;
        this.fecha = fecha;
        this.hora = hora;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSala_id() {
        return sala_id;
    }

    public void setSala_id(String sala_id) {
        this.sala_id = sala_id;
    }

    public String getPelicula_id() {
        return pelicula_id;
    }

    public void setPelicula_id(String pelicula_id) {
        this.pelicula_id = pelicula_id;
    }

    public String getFormatopelicula_id() {
        return formatopelicula_id;
    }

    public void setFormatopelicula_id(String formatopelicula_id) {
        this.formatopelicula_id = formatopelicula_id;
    }

    public String getFormato_lenguaje() {
        return formato_lenguaje;
    }

    public void setFormato_lenguaje(String formato_lenguaje) {
        this.formato_lenguaje = formato_lenguaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
