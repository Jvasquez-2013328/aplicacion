<!DOCTYPE html>
<html>
<head>
    <title>Editar formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('peliculas') }}">Peliculas</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('peliculas') }}">Ver peliculas</a></li>
                <li><a href="{{ URL::to('peliculas/create') }}">Agregar</a>
            </ul>
        </nav>

        <h1>Edit {{$pelicula_detail->nombre }}</h1>

        {{ HTML::ul($errors->all()) }}

        {{ Form::model($pelicula_detail, array('route' => array('peliculas.update', $pelicula_detail->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('titulo', 'Titulo') }}
            {{ Form::text('titulo', null, array('class' => 'form-control')) }}
            {{ Form::label('sinopsis', 'Sinopsis') }}
            {{ Form::text('sinopsis',null, array('class' => 'form-control')) }}
            {{ Form::label('trailer_url', 'Trailer url') }}
            {{ Form::text('trailer_url', null, array('class' => 'form-control')) }}
            {{ Form::label('image', 'Image') }}
            {{ Form::text('image', null, array('class' => 'form-control')) }}
            {{ Form::label('rated', 'Rated url') }}
            {{ Form::text('rated',null, array('class' => 'form-control')) }}
            {{ Form::label('genero', 'Genero') }}
            {{ Form::text('genero', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Terminar', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
    </body>
</html>