<!DOCTYPE html>
<html>
<head>
    <title>Agregar formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>Agregar formato</h1>

        <!-- if there are creation errors, they will show here -->
        {{ HTML::ul($errors->all()) }}

        {{ Form::open(array('url' => 'formato')) }}

        <div class="form-group">
            {{ Form::label('nombre', 'Nombre') }}
            {{ Form::text('nombre', Input::old('nombre'), array('class' => 'form-control')) }}
            {{ Form::label('descripcion', 'Descripcion') }}
            {{ Form::text('descripcion', Input::old('descripcion'), array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Agregar', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    </div>
    </body>
</html>