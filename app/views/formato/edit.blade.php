<!DOCTYPE html>
<html>
<head>
    <title>Editar formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{$formato_detail->nombre }}</h1>

        <!-- if there are creation errors, they will show here -->
        {{ HTML::ul($errors->all()) }}

        {{ Form::model($formato_detail, array('route' => array('formato.update', $formato_detail->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('nombre', 'Nombre') }}
            {{ Form::text('nombre', null, array('class' => 'form-control')) }}
            {{ Form::label('descripcion', 'Descripcion') }}
            {{ Form::text('descripcion', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Terminar', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
    </body>
</html>