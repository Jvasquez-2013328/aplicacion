<!DOCTYPE html>
<html>
<head>
    <title>Mostrar formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('formato') }}">Formato</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('formato') }}">Ver formatos</a></li>
                <li><a href="{{ URL::to('formato/create') }}">Agregar un formato</a>
            </ul>
        </nav>

        <h1>{{ $formato_detail->nombre }}</h1>

        <div class="jumbotron text-center">
            <h2>{{$formato_detail->nombre }}</h2>
            <p>
                <strong>Nombre:</strong> {{ $formato_detail->nombre }}<br>
            </p>
        </div>

    </div>
    </body>
</html>