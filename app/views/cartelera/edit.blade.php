<!DOCTYPE html>
<html>
<head>
    <title>Editando</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>Edit {{$cartelera_detail->nombre }}</h1>

        {{ HTML::ul($errors->all()) }}

        {{ Form::model($cartelera_detail, array('route' => array('cartelera.update', $cartelera_detail->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('sala_id', 'Sala id') }}
            {{ Form::text('sala_id', null, array('class' => 'form-control')) }}
            {{ Form::label('pelicula_id', 'Pelicula id') }}
            {{ Form::text('pelicula_id', null, array('class' => 'form-control')) }}
            {{ Form::label('formatopelicula_id', 'Formatopelicula id') }}
            {{ Form::text('formatopelicula_id', null, array('class' => 'form-control')) }}
            {{ Form::label('formato_lenguaje', 'Formato_lenguaje') }}
            {{ Form::text('formato_lenguaje', null, array('class' => 'form-control')) }}
            {{ Form::label('fecha', 'Fecha') }}
            {{ Form::text('fecha', null, array('class' => 'form-control')) }}
            {{ Form::label('hora', 'Hora') }}
            {{ Form::text('hora',null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Terminar', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
    </body>
</html>