<!DOCTYPE html>
<html>
<head>
    <title>Agregar nuevo tipo de sala</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-muted text-center">
                Proyecto 5 bimestre
            </h3>
        </div>
    </div>
</div>
{{ Form::open(array('url'=>'user/signin', 'class'=>'form-signin')) }}
<h2 class="form-signin-heading">Please Login</h2>
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}

{{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}
<hr>
    <a class="navbar-brand" href="{{ URL::to('user/register') }}">Registrar</a>
</body>
</html>