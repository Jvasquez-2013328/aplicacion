<!DOCTYPE html>
<html>
<head>
    <title>Editar tipo</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{$tipo_detail->nombre }}</h1>

        {{ HTML::ul($errors->all()) }}

        {{ Form::model($tipo_detail, array('route' => array('tipo.update', $tipo_detail->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('nombre', 'Nombre') }}
            {{ Form::text('nombre', null, array('class' => 'form-control')) }}
            {{ Form::label('descripcion', 'Descripcion') }}
            {{ Form::text('descripcion', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
    </body>
</html>