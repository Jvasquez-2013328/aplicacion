<!DOCTYPE html>
<html>
<head>
    <title>Editando</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <hr>

        <h1>Edit {{ $cine_detail->nombre }}</h1>

        <!-- if there are creation errors, they will show here -->
        {{ HTML::ul($errors->all()) }}

        {{ Form::model($cine_detail, array('route' => array('cines.update', $cine_detail->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('nombre', 'Nombre') }}
            {{ Form::text('nombre', null, array('class' => 'form-control')) }}
            {{ Form::label('direccion', 'Direccion') }}
            {{ Form::text('direccion', null, array('class' => 'form-control')) }}
            {{ Form::label('telefono', 'Telefono') }}
            {{ Form::text('telefono', null, array('class' => 'form-control')) }}
            {{ Form::label('latitud', 'Latitud') }}
            {{ Form::text('latitud', null, array('class' => 'form-control')) }}
            {{ Form::label('longitud', 'Longitud') }}
            {{ Form::text('longitud', null, array('class' => 'form-control')) }}
            {{ Form::label('hora_apertura', 'Hora apertura') }}
            {{ Form::text('hora_apertura', null, array('class' => 'form-control')) }}
            {{ Form::label('hora_cierre', 'Hora cierre') }}
            {{ Form::text('hora_cierre', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Editando', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
</body>
</html>