<!DOCTYPE html>
<html>
<head>
    <title>Agregar cines</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <hr>
    <h1>Crear cine</h1>

    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'cines')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', Input::old('nombre'), array('class' => 'form-control')) }}
        {{ Form::label('direccion', 'Direccion') }}
        {{ Form::text('direccion', Input::old('direccion'), array('class' => 'form-control')) }}
        {{ Form::label('telefono', 'Telefono') }}
        {{ Form::text('telefono', Input::old('telefono'), array('class' => 'form-control')) }}
        {{ Form::label('latitud', 'Latitud') }}
        {{ Form::text('latitud', Input::old('latitud'), array('class' => 'form-control')) }}
        {{ Form::label('longitud', 'Longitud') }}
        {{ Form::text('longitud', Input::old('longitud'), array('class' => 'form-control')) }}
        {{ Form::label('hora_apertura', 'Hora_apertura') }}
        {{ Form::text('hora_apertura', Input::old('hora apertura'), array('class' => 'form-control')) }}
        {{ Form::label('hora_cierre', 'Hora_cierre') }}
        {{ Form::text('hora_cierre', Input::old('hora cierre'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
</html>