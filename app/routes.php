<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::resource('cines','CineController');
Route::get('/', 'CineController@index');

Route::resource('cartelera','CarteleraController');
Route::get('/', 'carteleraController@index');

Route::resource('formato','FormatoController');
Route::get('/', 'FormatoController@index');

Route::resource('peliculas','PeliclasController');
Route::get('/', 'PeliclasController@index');

Route::resource('salas','SalasController');
Route::get('/', 'SalasController@index');

Route::resource('tipo','TipoSalasController');
Route::get('/', 'TipoSalasController@index');

Route::controller('user', 'UsersController');