<?php

class SalasController extends \BaseController {


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $nerds = Salas::all();

        return View::make('sala.index')
            ->with('Sala', $nerds);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('sala.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'cine_id' => 'required',
            'numero' => 'required',
            'tiposala_id' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('salas/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new Salas();
            $nerds->cine_id = Input::get('cine_id');
            $nerds->numero = Input::get('numero');
            $nerds->tiposala_id = Input::get('tiposala_id');
            $nerds->save();

            return Redirect::to('salas');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $form = Salas::find($id);

        // show the view and pass the nerd to it
        return View::make('sala.show')
            ->with('sala_detail', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $nerds = Salas::find($id);

        // show the edit form and pass the nerd
        return View::make('sala.edit')
            ->with('sala_detail', $nerds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'cine_id' => 'required',
            'numero' => 'required',
            'tiposala_id' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('salas/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $nerds = Salas::find($id);
            $nerds->cine_id = Input::get('cine_id');
            $nerds->numero = Input::get('numero');
            $nerds->tiposala_id = Input::get('tiposala_id');
            $nerds->save();

            return Redirect::to('salas');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $nerds = Salas::find($id);
        $nerds->delete();

        return Redirect::to('salas');
    }


}
