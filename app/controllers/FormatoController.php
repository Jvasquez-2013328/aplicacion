<?php

class FormatoController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $nerds = FormatoPelicula::all();

        return View::make('formato.index')
            ->with('FormatoPelicula', $nerds);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return View::make('formato.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('formato/create')
                ->withErrors($validator);
        } else {
            // store
            $nerds = new FormatoPelicula();
            $nerds->nombre = Input::get('nombre');
            $nerds->descripcion = Input::get('descripcion');
            $nerds->save();

            return Redirect::to('formato');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $form = FormatoPelicula::find($id);

        // show the view and pass the nerd to it
        return View::make('formato.show')
            ->with('formato_detail', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $nerds = FormatoPelicula::find($id);

        // show the edit form and pass the nerd
        return View::make('formato.edit')
            ->with('formato_detail', $nerds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('formato/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $nerds = FormatoPelicula::find($id);
            $nerds->nombre = Input::get('nombre');
            $nerds->descripcion = Input::get('descripcion');
            $nerds->save();

            return Redirect::to('formato');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $nerds = FormatoPelicula::find($id);
        $nerds->delete();

        return Redirect::to('formato');
    }

}
