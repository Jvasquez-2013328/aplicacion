CREATE DATABASE CineKinal;

USE CineKinal;


CREATE TABLE Pelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(100) NOT NULL,
    sinopsis VARCHAR(500) NOT NULL,
    trailer_url VARCHAR(200) NOT NULL,
    image TEXT NULL,
		fechaEstreno VARCHAR(50) NOT NULL,
    rated VARCHAR(20) NOT NULL,
    genero VARCHAR(50) NOT NULL
);

CREATE TABLE Favorito(
		idPelicula INT NOT NULL,
		idUsuario VARCHAR(100) NULL,
		PRIMARY KEY(idPelicula, idUsuario),
    FOREIGN KEY (idPelicula) REFERENCES Pelicula(id),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(id)

);

CREATE TABLE FormatoPelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Cine(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    direccion VARCHAR(500) NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    latitud DOUBLE(20, 18) NOT NULL,
    longitud DOUBLE(20, 18) NOT NULL,
    hora_apertura VARCHAR(50) NOT NULL,
    hora_cierre VARCHAR(50) NOT NULL
);


CREATE TABLE TipoSala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
  descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Sala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cine_id INT NOT NULL,
    numero INT NOT NULL,
    tiposala_id INT NOT NULL,
    FOREIGN KEY (cine_id) REFERENCES Cine(id),
    FOREIGN KEY (tiposala_id) REFERENCES TipoSala(id)
);



CREATE TABLE Cartelera(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sala_id INT NOT NULL,
    pelicula_id INT NOT NULL,
    formatopelicula_id INT NOT NULL,
    formato_lenguaje VARCHAR(10) NOT NULL,
    fecha VARCHAR(50) NOT NULL,
    hora VARCHAR(50) NOT NULL,
    FOREIGN KEY (sala_id) REFERENCES Sala(id),
    FOREIGN KEY (pelicula_id) REFERENCES Pelicula(id),
    FOREIGN KEY (formatopelicula_id) REFERENCES FormatoPelicula(id)
);



INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('La pasion de Cristo', 'Año 30 de nuestra era. En la provincia romana de Judea, un misterioso carpintero llamado Jesús de Nazareth comienza a anunciar la llegada del "reino de Dios" y se rodea de un grupo de humildes pescadores: los Apóstoles. Durante siglos, el pueblo judío había esperado la llegada del Mesías - personaje providencial que liberaría su sagrada patria e instauraría un nuevo orden basado en la justicia-. Las enseñanzas de Jesús atraen a una gran multitud de seguidores que lo reconocen como el Mesías. Alarmado por la situación, el Sanedrín, con la ayuda de Judas Iscariote, uno de los doce Apóstoles, arresta a Jesús. Acusado de traición a Roma, Cristo es entregado a Poncio Pilato, quien, para evitar un motín, lo condena a a morir en la cruz como un vulgar criminal.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://buenavistafilms.com/wp-content/uploads/2010/12/La-pasion-de-Cristo.jpg", "05/07/2015", 'A', 'Cristiana');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Dragon Ball Z: La resurrección de Freezer', 'Después de que Bills, el Dios de la destrucción, decidiera no destruir la Tierra, se vive una gran época de paz. Hasta que Sorbet y Tagoma, antiguos miembros élite de la armada de Freezer, llegan a la Tierra con el objetivo de revivir a su líder por medio de las Bolas de Dragón. Su deseo es concedido y ahora Freezer planea su venganza en contra de los Saiyajin. La historia hace que una gran oleada de hombres bajo el mando de Freezer lo acompañe.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://i.blogs.es/d378b6/dragon-ball/450_1000.jpg", "06/07/2015", 'B', 'Anime');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Ted 2', 'Recién casados, Ted y Tami-Lynn quieren tener un bebe. Pero antes de ser padre, Ted tendrá que demostrar ante un tribunal de justicia que es una persona.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://1.bp.blogspot.com/-EZbBWBAnfyI/U9jmEULaScI/AAAAAAAAAEQ/0VjVUenf4XI/s1600/Ted+2+Movie.jpg", "27/07/2015", 'C', 'Comedia');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Los Minions', 'La historia de Los Minions se remonta al principio de los tiempos. Empezaron siendo organismos amarillos unicelulares que evolucionaron a través del tiempo, poniéndose siempre al servicio de los amos más despreciables. Ante su incapacidad para mantener a esos amos – desde el T. Rex a Napoleón –, los Minions acaban encontrándose solos y caen en una profunda depresión. Sin embargo, uno de ellos, llamado Kevin, tiene un plan. Acompañado por el rebelde Stuart y el adorable Bob, emprende un emocionante viaje para conseguir una jefa a quien servir, la terrible Scarlet Overkill. Pasarán de la helada Antártida, a la ciudad de Nueva York en los años sesenta, para acabar en el Londres de la misma época, donde deberán enfrentarse al mayor reto hasta la fecha: salvar a la raza Minion de la aniquilación. ', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://www.ecartelera.com/carteles/6700/6719/002_c580ci.jpg", "05/07/2015", 'D', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Big Hero 6', 'Situado en una metrópolis ficticia llamada San Fransokyo (una mezcla de San Francisco y Tokio), Hiro Hamada es un chico prodigio de 14 años, que ha creado un robot llamado Baymax para llenar el vacío que le dejó la muerte de su hermano Tadashi, mientras éste estaba a punto de descubrir una red criminal liderada por un misterioso villano enmascarado llamado Yokai. Para encontrar al asesino y detener a los criminales, Hiro decide formar un equipo de poderosos superhéroes llamado "Big Hero 6", que incluye a Wasabi-No-Ginger, Honey Lemon, GoGo Tomago, Fred, Baymax y a él mismo. Adaptación animada del cómic de Marvel.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://blogs-images.forbes.com/scottmendelson/files/2014/10/14m5eu1.jpg", "10/07/2015", 'E', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Rápidos y Furiosos 7', 'El director James Wan, encargado de películas como Saw o Insidious coge el relevo de Justin Lin y se pone al cargo de la séptima entrega de esta saga sobre ruedas protagonizada por Vin Diesel (Las crónicas de Riddick).De nuevo los problemas les persiguen. Sin conseguir librarse de la mancha de criminales de su ficha. Su pasado de larga trayectoria en las carreras ilegales les perseguirá y deberán hacer frente a las circunstancias de la única forma que saben. Cuando les pongan entre la espada y la pared idearán un plan para escapar del peligro en una trepidante carrera por salvar la vida y limpiar su reputación. Acción, persecuciones y un ritmo frenético en esta nueva entrega.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://i.ytimg.com/vi/k7d1NruQfI0/maxresdefault.jpg", "15/07/2015", 'H', 'Acción');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('En busca de la felicidad', 'Chris Gardner (Will Smith) es un vendedor brillante y con talento, pero su empleo no le permite cubrir sus necesidades más básicas. Tanto es así que acaban echándolo, junto a su hijo de cinco años (Jaden Smith), de su piso de San Francisco, y ambos no tienen ningún lugar al que ir. Cuando Gardner consigue hacer unas prácticas en una prestigiosa correduría de bolsa, los dos protagonistas tendrán que afrontar muchas adversidades parar hacer realidad su sueño de una vida mejor. ', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://frasesdelapelicula.com/wp-content/uploads/2010/08/en-busqueda-de-la-felicidad.jpg", "11/07/2015", 'I', 'Drama');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Terremoto: La falla de San Andrés', 'La falla de San Andrés acaba cediendo ante las temibles fuerzas telúricas y desencadena un terremoto de magnitud 9 en California. Ante tal catástrofe, el piloto de helicóptero de búsqueda y rescate Ray (Dwayne Johnson) y su ex esposa Emma (Carla Gugino) viajan juntos desde Los Ángeles hasta San Francisco para salvar a su única hija, Blake (Alexandra Daddario). Pero su tortuoso viaje hacia el norte solamente es el comienzo del desomoronamiento de todo lo que creían firme en su vida.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://lgblog.cl.s3.amazonaws.com/wp-content/uploads/2015/05/sa03.jpg", "05/07/2015", 'K', 'Aventura');


INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Pixels', 'nos extraterrestres malinterpretan las imágenes de las máquinas recreativas como una declaración de guerra y deciden atacar la Tierra, empleando dichos juegos como modelos para el asalto. El presidente de EEUU, Will Cooper (Kevin James), recurre entonces a su gran amigo de la infancia y campeón de las maquinitas de los años 80, Sam Brenner (Adam Sandler), quien actualmente trabaja como instalador de sistemas de home cinema, para encabezar un equipo de expertos jugadores de su época (Monaghan, Dinklage y Gad). Su misión será derrotar a los alienígenas y salvar al planeta.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://cdn.traileraddict.com/content/columbia-pictures/pixels-poster-2.jpg", "31/08/2015", 'G', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Jurassic Park IV', 'Nueva entrega de la saga iniciada por Steven Spielberg. Veintidós años después de lo ocurrido en Jurassic Park, la isla Nublar ha sido transformada en un parque temático, Jurassic Wold, con versiones «domesticadas» de algunos de los dinosaurios más conocidos. Cuando todo parece ir a la perfección y ser el negocio del siglo, un nuevo dinosaurio de especie todavía desconocida y que es mucho más inteligente de lo que se pensaba, comienza a causar estragos entre los habitantes del Parque.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://www.scifi-movies.com/images/contenu/data/0004566/affiche-pixels-2015-5.jpg", "31/08/2015", 'K', ' Aventuras');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Smosh: The Movie', 'Ian Hecox y Anthony Padilla deben viajar por todo el mundo de Youtube para conseguir borrar un humillante vídeo protagonizado por uno de los dos miembros de Smosh.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://cdn.smoshthemovie.com/wp-content/uploads/2015/05/movie-poster.jpeg", "15/08/2015", 'G', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Tinker Bell y La Bestia de Nunca Jamás ', 'nos extraterrestres malinterpretan las imágenes de las máquinas recreativas como una declaración de guerra y deciden atacar la Tierra, empleando dichos juegos como modelos para el asalto. El presidente de EEUU, Will Cooper (Kevin James), recurre entonces a su gran amigo de la infancia y campeón de las maquinitas de los años 80, Sam Brenner (Adam Sandler), quien actualmente trabaja como instalador de sistemas de home cinema, para encabezar un equipo de expertos jugadores de su época (Monaghan, Dinklage y Gad). Su misión será derrotar a los alienígenas y salvar al planeta.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://cineguate.com/gt/wp-content/uploads/2015/02/spotlight-TINKERBELL-710x380.jpg", "31/08/2015", 'G', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Asterix: La residencia de los Dioses ', 'Julio César tiene la idea de construir una urbanización de lujo junto a la aldea gala de Astérix, para que la pequeña comunidad acabe sucumbiendo a la atracción de la modernidad y abandone su lucha contra los romanos. Para desgracia de los inquilinos de la Residencia de los Dioses, finca de varios pisos, donde se alojan ciudadanos de clase media, y construída con materiales baratos), Asurancetúrix se muda a uno de los pisos, provocando el descontento y numerosas quejas.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://elcineriodico.com/wp-content/uploads/2015/03/Asterix-La-residencia-de-los-Dioses-Poster.jpg", "31/08/2015", 'G', 'Animacion');
INSERT INTO Pelicula(titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero) VALUES ('Intensamente ', 'nos extraterrestres malinterpretan las imágenes de las máquinas recreativas como una declaración de guerra y deciden atacar la Tierra, empleando dichos juegos como modelos para el asalto. El presidente de EEUU, Will Cooper (Kevin James), recurre entonces a su gran amigo de la infancia y campeón de las maquinitas de los años 80, Sam Brenner (Adam Sandler), quien actualmente trabaja como instalador de sistemas de home cinema, para encabezar un equipo de expertos jugadores de su época (Monaghan, Dinklage y Gad). Su misión será derrotar a los alienígenas y salvar al planeta.', 'https://es.wikipedia.org/wiki/La_Pasi%C3%B3n_de_Cristo_%28pel%C3%ADcula%29', "http://www.cinemas.com.ni/wp-content/uploads/2015/05/poster-intensamente.jpg", "31/08/2015", 'G', 'Animacion');



INSERT INTO FormatoPelicula(nombre, descripcion) VALUES ('2D','Sonido Envolvente');
INSERT INTO FormatoPelicula(nombre, descripcion) VALUES ('3D','En otra dimencion');
INSERT INTO FormatoPelicula(nombre, descripcion) VALUES ('4D','Otra aventura');



INSERT INTO TipoSala(nombre, descripcion) VALUES('Pequeña','50 Personas');
INSERT INTO TipoSala(nombre, descripcion) VALUES('Mediana','100 Personas');
INSERT INTO TipoSala(nombre, descripcion) VALUES('Grande','150 Personas');



INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Miraflores', 'zona 7', '55889977', '3.000', '4.000', '9:00', '22:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Tikal', 'zona 11', '55889977', '3.000', '4.000', '8:00', '21:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Portales', 'zona 18', '55889977', '3.000', '4.000', '9:00', '21:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Galerias Prima', 'zona 7', '55889977', '3.000', '4.000', '10:00', '22:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Metro Norte', 'zona 7', '55889977', '3.000', '4.000', '9:00', '23:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Plaza Coban', 'Coban', '55889977', '3.000', '4.000', '7:00', '20:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Los Proceres', 'zona 13', '55889977', '3.000', '4.000', '10:00', '23:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Los Capitols', 'zona 1', '55889977', '3.000', '4.000', '9:00', '22:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Cine Lux', 'zona 1', '55889977', '3.000', '4.000', '7:00', '21:00');
INSERT INTO Cine(nombre, direccion, telefono, latitud, longitud, hora_apertura, hora_cierre) VALUES ('Gran Via', 'zona 9', '55889977', '3.000', '4.000', '9:00', '23:00');



INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('1', '1', '1' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('2', '2', '2' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('3', '3', '3' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('4', '4', '4' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('5', '5', '5' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('6', '6', '6' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('7', '7', '7' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('8', '8', '8' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('9', '9', '9' );
INSERT INTO Sala(cine_id, numero, tiposala_id) VALUES ('10', '10', '10' );





INSERT INTO Cartelera(sala_id, pelicula_id, formatopelicula_id, formato_lenguaje, fecha, hora) VALUES ('1', '1', '1', 'Español', "05/04/2015", '4:00');
INSERT INTO Cartelera(sala_id, pelicula_id, formatopelicula_id, formato_lenguaje, fecha, hora) VALUES ('2', '2', '2', 'Ingles', "05/04/2015", '4:00');
INSERT INTO Cartelera(sala_id, pelicula_id, formatopelicula_id, formato_lenguaje, fecha, hora) VALUES ('3', '3', '3', 'Portugues', "05/04/2015", '4:00');

